# Dius Shopping Exercise
 ![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)

## Installation

You will need to install `node` globally. Once done, you can use `npm` to install the dependencies and then start the program by `npm start`.

```bash
# Get the latest snapshot
git clone https://gitlab.com/ronakmpatel/dius-shopping.git dius-shopping

# Change directory
cd shopping

# Install NPM dependencies
npm install

# Start the simulator
npm start

# Test the simulator
npm test
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the program and outputs the total in console.

### `npm test`

Project is 100% tested with TDD.
## Key Files

The program is divided in to three parts:

- scan products
- apply discount on the order
- calculateTotal

`index.js` is responsible for scanning products and it is the entry point for the program. You can scan product with

```Javascript
// Scan Apple TV
scan("atv")
// Scan MacBook Pro
scan("mbp")
//scan VGA
scan("vga")
//scan iPad
scan("ipd")
```

You can scan multiple same items by calling scan function multiple times with same sku.

```Javascript
scan("mbp")
scan("mbp")
scan("mbp")
```

You can use `npm start` to see the total amount. Please note that you do not need to scan vga
when you scan the MacBook Pro. Offer handler will automatically add it to the  cart.

Scan handler will ignore the invalid sku. You can only scan products available in the `productLookup.js`.

* `offers.js` This file contains all the offers handler.

* `productLookup.js` This file contain simple hash lookup. You can pass sku to this handler to get more information about the product such as name, and price.

* `calculateTotal.js` This file is responsible applying offers to the order and calculate the total amount.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the program with the `input.txt` file input.

### `npm test`

Project is 100% tested with TDD.

### License

Shopping is MIT licensed.
