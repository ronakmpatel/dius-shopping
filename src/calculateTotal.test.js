const { calculateTotal } = require("./calculateTotal");

describe("calculateTotal", () => {
  it("applies offer correctly for example scenario 1", () => {
    const ORDER = ["atv", "atv", "atv", "vga"]
    const EXPECTED_TOTAL = 249
    expect(calculateTotal(ORDER)).toBe(EXPECTED_TOTAL)
  })
  it("applies offer correctly for example scenario 1", () => {
    const ORDER = ["atv", "ipd", "ipd", "atv", "ipd", "ipd", "ipd"]
    const EXPECTED_TOTAL = 2718.95
    expect(calculateTotal(ORDER)).toBe(EXPECTED_TOTAL)
  })
  it("applies offer correctly for example scenario 1", () => {
    /* Note: The vga will be automatically added to the cart. */
    const ORDER = ["mbp", "ipd"]
    const EXPECTED_TOTAL = 1949.98
    expect(calculateTotal(ORDER)).toBe(EXPECTED_TOTAL)
  })
})