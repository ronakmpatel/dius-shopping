const { calculateTotal } = require("./calculateTotal");

const order = [];

const scanToCurry = (order) => (sku) => {
  order.push(sku);
};

const scan = scanToCurry(order)

// Example usage.
scan("atv");
scan("mbp");
scan("vga");
scan("ipd");

const total = calculateTotal(order)
console.log(total);

module.exports = { scanToCurry }