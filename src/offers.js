const { productLookup } = require("./productLookup");

/**
 * @todo refactor the name rule to something sensible.
 *
 * This function will apply multi buy discount for the given
 * total quantities bought and the rule apply to it.
 * For example: for buy 3 for 2 deal you can provide 3 as rule.
 * @param {number} totalQuantity total quantity for this product
 * @param {number} rule rule apply to find free quantities
 */
const calculateFreeQuantity = (totalQuantity, rule) => {
  const freeQuantity = Math.floor(totalQuantity / rule);
  //   const normalPriceToCharge = Math.floor(quantity - freeQuantity);
  return freeQuantity;
};

const multiBuyOffer = (cart, productList, rule) => {
  const freeQuantity = calculateFreeQuantity(productList.length, rule);
  productList.map((product, index) => {
    if (index < freeQuantity) {
      const FREE = 0;
      addProductToCart(cart, product, FREE);
    } else {
      addProductToCart(cart, product, productLookup(product).price);
    }
  });
};

const addFreeProduct = (cart, freeProductName) => {
  const FREE = 0;
  cart.push({ productName: freeProductName, price: FREE });
};

const addProductToCart = (cart, sku, price) => {
  cart.push({ productName: productLookup(sku).name, price });
};

const buyOneGetSomethingFree = (cart, productList, freeProductSku) => {
  productList.map(product => {
    productLookup("mbp").name; //?
    addProductToCart(cart, product, productLookup(product).price);
    addFreeProduct(cart, productLookup(freeProductSku).name);
  });
};

const bulkDiscount = (cart, productList, discountedPrice) => {
  productList.map(product => {
    addProductToCart(cart, product, discountedPrice);
  });
};

module.exports = {
  multiBuyOffer,
  addFreeProduct,
  addProductToCart,
  buyOneGetSomethingFree,
  bulkDiscount,
  calculateFreeQuantity
};
