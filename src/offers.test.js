const {
  multiBuyOffer,
  addFreeProduct,
  addProductToCart,
  calculateFreeQuantity,
  buyOneGetSomethingFree,
  bulkDiscount
} = require("./offers");

describe("offers", () => {
  describe("calculateFreeQuantity ()", () => {
    it("correctly calculates the free quantities for buy 3 for 2 offer", () => {
      const totalQuantity = 10;
      const rule = 3;
      const expectedFreeQuantities = 3;
      expect(calculateFreeQuantity(totalQuantity, rule)).toEqual(
        expectedFreeQuantities
      );
    });
    it("correctly calculates the free quantities for buy 4 for 2 offer", () => {
      const totalQuantity = 10;
      const rule = 4;
      const expectedFreeQuantities = 2;
      expect(calculateFreeQuantity(totalQuantity, rule)).toEqual(
        expectedFreeQuantities
      );
    });
    it("correctly calculates the free quantities for buy 7 for 5 offer", () => {
      const totalQuantity = 10;
      const rule = 7;
      const expectedFreeQuantities = 1;
      expect(calculateFreeQuantity(totalQuantity, rule)).toEqual(
        expectedFreeQuantities
      );
    });
    it("correctly calculates the free quantities for no discount", () => {
      const totalQuantity = 2;
      const rule = 3;
      const expectedFreeQuantities = 0;
      expect(calculateFreeQuantity(totalQuantity, rule)).toEqual(
        expectedFreeQuantities
      );
    });
  });
  describe("addFreeProduct ()", () => {
    it("adds the free product to given empty cart", () => {
      const CART = [];
      const EXPECTED_CART = [{ productName: "VGA Adapter", price: 0 }];
      addFreeProduct(CART, "VGA Adapter");
      expect(CART).toEqual(EXPECTED_CART);
    });
    it("adds the free product to given cart", () => {
      const CART = [{ productName: "Apple TV", price: 109.5 }];
      const EXPECTED_CART = [
        { productName: "Apple TV", price: 109.5 },
        { productName: "VGA Adapter", price: 0 }
      ];
      addFreeProduct(CART, "VGA Adapter");
      expect(CART).toEqual(EXPECTED_CART);
    });
  });
  describe("addProductToCart ()", () => {
    it("adds the product to given empty cart", () => {
      const CART = [];
      const EXPECTED_CART = [{ productName: "VGA adapter", price: 30 }];
      addProductToCart(CART, "vga", 30);
      expect(CART).toEqual(EXPECTED_CART);
    });
    it("adds the product to given cart", () => {
      const CART = [
        { productName: "Apple TV", price: 109.5 },
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 30 }
      ];
      const EXPECTED_CART = [
        { productName: "Apple TV", price: 109.5 },
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 30 },
        { productName: "VGA adapter", price: 30 }
      ];
      addProductToCart(CART, "vga", 30);
      expect(CART).toEqual(EXPECTED_CART);
    });
  });
  describe("multiBuyOffer ()", () => {
    it("correctly calculates the offer 3 for 2 offer for empty cart", () => {
      const CART = [];
      const order = ["atv", "atv", "atv"];
      const RULE = 3;
      const EXPECTED_CART = [
        { productName: "Apple TV", price: 0 },
        { productName: "Apple TV", price: 109.5 },
        { productName: "Apple TV", price: 109.5 }
      ];
      multiBuyOffer(CART, order, RULE);
      expect(CART).toEqual(EXPECTED_CART);
    });
    it("correctly calculates the offer 3 for 2 offer", () => {
      const CART = [{ productName: "VGA adapter", price: 30 }];
      const ORDER = ["atv", "atv", "atv"];
      const RULE = 3;
      const EXPECTED_CART = [
        { productName: "VGA adapter", price: 30 },
        { productName: "Apple TV", price: 0 },
        { productName: "Apple TV", price: 109.5 },
        { productName: "Apple TV", price: 109.5 }
      ];
      multiBuyOffer(CART, ORDER, RULE);
      expect(CART).toEqual(EXPECTED_CART);
    });
  });
  describe("buyOneGetSomethingFree", () => {
    it("correctly calculates the offer for empty cart", () => {
      const CART = [];
      const ORDER = ["mbp", "mbp"];
      const FREE_PRODUCT_SKU = "vga";
      const EXPECTED_CART = [
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 0 },
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 0 }
      ];
      buyOneGetSomethingFree(CART, ORDER, FREE_PRODUCT_SKU);
      expect(CART).toEqual(EXPECTED_CART);
    });
    it("correctly calculates the offer", () => {
      const CART = [{ productName: "Super iPad", price: 549.99 }];
      const ORDER = ["mbp", "mbp", "mbp"];
      const FREE_PRODUCT_SKU = "vga";
      const EXPECTED_CART = [
        { productName: "Super iPad", price: 549.99 },
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 0 },
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 0 },
        { productName: "MacBook Pro", price: 1399.99 },
        { productName: "VGA adapter", price: 0 }
      ];
      buyOneGetSomethingFree(CART, ORDER, FREE_PRODUCT_SKU);
      expect(CART).toEqual(EXPECTED_CART);
    });
  });
  describe("bulkDiscount ()", () => {
      it("correctly sets the discount for the empty cart", () => {
        const CART = []
        const ORDER = ["ipd", "ipd", "ipd"];
        const EXPECTED_CART = [
          { productName: "Super iPad", price: 499.99 },
          { productName: "Super iPad", price: 499.99 },
          { productName: "Super iPad", price: 499.99 }
        ];
        const DISCOUNTED_PRICE = 499.99
        bulkDiscount(CART, ORDER, DISCOUNTED_PRICE)
        expect(CART).toEqual(EXPECTED_CART)
      })
      it("correctly sets the discount for the cart", () => {
        const CART = [
            { productName: "MacBook Pro", price: 1399.99 },
            { productName: "VGA adapter", price: 0 }
        ]
        const ORDER = ["ipd", "ipd", "ipd"];
        const EXPECTED_CART = [
          { productName: "MacBook Pro", price: 1399.99 },
          { productName: "VGA adapter", price: 0 },
          { productName: "Super iPad", price: 499.99 },
          { productName: "Super iPad", price: 499.99 },
          { productName: "Super iPad", price: 499.99 }
        ];
        const DISCOUNTED_PRICE = 499.99
        bulkDiscount(CART, ORDER, DISCOUNTED_PRICE)
        expect(CART).toEqual(EXPECTED_CART)
      })
  })
});
