const { buyOneGetSomethingFree, multiBuyOffer, addProductToCart, bulkDiscount } = require("./offers");
const {
  productLookup
} = require("./productLookup");

const calculateTotal = order => {
  const cart = [];
  const sum = 0
  const mbpOrders = order.filter(order => order === "mbp");
  if (mbpOrders.length) {
    buyOneGetSomethingFree(cart, mbpOrders, "mbp", "vga")
  }
  const atvOrders = order.filter(order => order === "atv");
  multiBuyOffer(cart, atvOrders, 3)

  const ipadOrders = order.filter(order => order === "ipd");
  if (ipadOrders.length > 4) {
    bulkDiscount(cart, ipadOrders, 499.99)
  } else {
    ipadOrders.map((ipad) => addProductToCart(cart, ipad, productLookup(ipad).price))
  }
  const vgaOrders = order.filter(order => order === "vga");
  vgaOrders.map((vga) => addProductToCart(cart, vga, productLookup(vga).price))
  
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const total = cart.reduce((accumulator, currentValue) => reducer(accumulator, currentValue.price), sum)
  return total
};

module.exports = {
  calculateTotal
};
