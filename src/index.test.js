const {
    scanToCurry
  } = require("./index");

  describe("scan ()", () => {
      it("correctly scans the product", () => {
          const order = []
          const itemsToScan = ["atv", "atv", "atv", "vga"]
          const expectedOrder = ["atv", "atv", "atv", "vga"]
          const scan = scanToCurry(order)
          itemsToScan.map((item) => {
              scan(item)
          })
          expect(order).toEqual(expectedOrder)
      })
  })