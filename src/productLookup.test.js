const {
    productLookup
  } = require("./productLookup");

  describe("productLookup ()", () => {
    it("returns correct product for ipad sku", () => {
      const SKU = "ipd";
      expect(productLookup(SKU)).toEqual({
        sku: "ipd",
        name: "Super iPad",
        price: 549.99
      });
    });
  
    it("returns correct product for mbp sku", () => {
      const SKU = "mbp";
      expect(productLookup(SKU)).toEqual({
        sku: "mbp",
        name: "MacBook Pro",
        price: 1399.99
      });
    });
  
    it("returns correct product for atv sku", () => {
      const SKU = "atv";
      expect(productLookup(SKU)).toEqual({
        sku: "atv",
        name: "Apple TV",
        price: 109.5
      });
    });
  
    it("returns correct product for vga sku", () => {
      const SKU = "vga";
      expect(productLookup(SKU)).toEqual({
        sku: "vga",
        name: "VGA adapter",
        price: 30
      });
    });
  
    it("returns undefined for invalid sku", () => {
      const INVALID_SKU = "invalid";
      expect(productLookup(INVALID_SKU)).toEqual(undefined);
    });
  });
